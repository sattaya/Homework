const fs = require('fs');


function percentage(num, per) {
    return (num/100)*per;
}

function calculateSalary(baseSalary) {
    let salary = [];
    salary[0] = parseInt(baseSalary);
    salary[1] = salary[0] + percentage(salary[0], 10);
    salary[2] = salary[1] + percentage(salary[1], 10);
    return salary;
}

function addYearSalary(row) {
    row.yearSalary = row.salary * 12;
}

function addNextSalary(row) {
    row.nextSalary = calculateSalary(row.salary);
}

function addAdditionalFields(data) {
    data.forEach(employee => {
        addYearSalary(employee);
        addNextSalary(employee);
    });
}
let employees = fs.readFileSync('homework1.json', 'utf8');
    employees = JSON.parse(employees);
    addAdditionalFields(employees);
    console.log(employees);
