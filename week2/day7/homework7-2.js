const fs = require('fs');

function percentage(num, per)
{
    return (num/100)*per;
}

function calculateSalary(baseSalary) {
    let salary = [];
    salary[0] = parseInt(baseSalary);
    salary[1] = salary[0] + percentage(salary[0], 10);
    salary[2] = salary[1] + percentage(salary[1], 10);
    return salary;
}

function addYearSalary(row) {
    row.yearSalary = row.salary * 12;
}

function addNextSalary(row) {
    row.nextSalary = calculateSalary(row.salary);
}

function addAdditionalFields(data) {
    let dataCopy = data.map(employee =>{
        return {...employee};
    })
    let newArr = dataCopy.map(employee => {
        addYearSalary(employee);
        addNextSalary(employee);
        //return Object.assign({}, employee);
        return employee; // ก๊อปปี้ข้างในทั้งหมดลง_อ๊อปเจคใหม่ {...employee,คีย์ที่ต้องการเปลี่ยนค่า = ค่าที่ต้องการเปลี่ยนs};
    });
    return newArr;
}

let employees = fs.readFileSync('homework1.json', 'utf8');
employees = JSON.parse(employees);
let newEmployees = addAdditionalFields(employees) 

    newEmployees[0].salary = 0;
    console.log(employees);
    console.log(newEmployees);