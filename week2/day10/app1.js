
const Koa = require('koa');
const app = new Koa();
const render = require('koa-ejs');
const path = require('path');

//-----------------ConnectDatabase---------------------------//
// get the client 
const mysql = require('mysql2/promise'); 
//----------------------render Use---------------------------//

render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false,
    debug: true
});

app.use(async(ctx, next) => {
    try {
            const connection = await mysql.createConnection({
                host: 'localhost',
                user: 'root',
                database: 'codecamp'
            });
            const [rows, fields] = await connection.execute('SELECT * FROM user');
        await ctx.render('homework10_1',{"rows":rows});
        await next();
    } catch (err) {
        ctx.status = 400
        ctx.body = `Uh-oh: ${err.message}`
    }
});

//----------------------------------------------//

app.listen(3001);