const qury = require('../models/homework10_1.js');
module.exports = function(app){
        app.use(async(ctx, next) => {
        try {
            const [rows,fields] = await qury.allUser()

            await ctx.render('homework10_1',{"rows":rows});
            await next();
        } catch (err) {
            ctx.status = 400
            ctx.body = `Uh-oh: ${err.message}`
        }
    });
}