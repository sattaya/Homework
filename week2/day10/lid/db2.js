class Database{
    constructor(){
        this.connectDatabase();

    }

    async connectDatabase(){
        const mysql = require('mysql2/promise'); 
        this.connection = await mysql.createConnection({
            host: 'localhost',
            user: 'root',
            database: 'codecamp'
        });
       // const [rows, fields] = await this.connection.execute('SELECT * FROM user');
    }
   execute(query) {
        return this.connection.execute(query);
    }

}
module.exports.db = new Database();