const koa = require('koa');
const app = new koa();
const render = require('koa-ejs');
const path = require('path');
const {db} = require('./lid/db2.js')

render(app,{
    root: path.join(__dirname,'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false,
    debug: true
});

app.use(async(ctx, next) => {
    try {
        const [rows,fields] = await db.execute('SELECT * FROM user')
        await ctx.render('homework10_1',{"rows":rows});
        await next();
    } catch (err) {
        ctx.status = 400
        ctx.body = `Uh-oh: ${err.message}`
    }
});

app.listen(3000);