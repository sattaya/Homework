const koa = require('koa');
const app = new koa();
const render = require('koa-ejs');
const path = require('path');
require('./controller/routes.js')(app);

render(app,{
    root: path.join(__dirname,'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false,
    debug: true
});




app.listen(3000);