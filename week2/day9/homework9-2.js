const {CEO} = require('./CEO.js')
const {Employee} = require('./Employee.js')
const {Programmer} = require('./Programmer.js')
const {OfficeCleaner} = require('./OfficeCleaner.js')

let somchai = new CEO("Somchai", "Sudlor", 30000, '00000', 'tshirt');
let somsi = new OfficeCleaner('somsi', 'thiland', 50000, '0000', 'maid')

somchai.readData('./employee9.json', (error, create) => {
    if (error) {
        console.error(error)
    } else {

        for (let i in create) {
            create[i].work(somsi)
        }
    }
})