const { Employee } = require("./Employee");

class Programmer extends Employee{
    constructor(firstname,lastname,salary,id,type){
        super(firstname,lastname,salary)
        this.id = id
        this.type = type
    }
    work(){
        this.webDesig()
        this.fixComputer()
        this.installWindows()
        
    }
    webDesig(){
        console.log('สร้างเว็บไซต์...')
     }
    fixComputer(){ 
        console.log('ซ่อมคอม...')
    }
    installWindows(){ 
        console.log('ลงวินโด้...')
    }
}

exports.Programmer = Programmer