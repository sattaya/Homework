const {Employee} = require('./Employee.js')


class OfficeCleaner extends Employee{
    constructor(firstname,lastname,salary,id,dressCode){
        super(firstname,lastname,salary)
        this.id = id
        this.dressCode = dressCode
    }
    work(employee) {
        this.Clean(employee);
        this.KillCoachroach();
        this.DecorateRoom();
        this.WelcomeGuest();
    }

    Clean(employee){
        console.log(`${employee.firstname} Clean`);
    }
    KillCoachroach(){
        console.log("KillCoachroach");
    }
    DecorateRoom(){
        console.log("DecorateRoom");
    }
    WelcomeGuest(){
        console.log("WelcomeGuest");
    }


}
exports.OfficeCleaner =  OfficeCleaner
