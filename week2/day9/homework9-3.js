const _ = require('lodash');

class MyUtility {
    assign(object1, sources) {
        return _.assign(object1, sources);

    };

    times(n, functionType) {
        return _.iteration(n, functionType);
    };

    keyBy(collection, functionType) {
        return _.keyBy(collection, functionType);
    };

    cloneDeep(value) {
       return _.cloneDeep(value);
    };

    filter(collection, functionType) {
        return _.filter(collection, functionType);
    };

    sortBy(collection, functionType) {
        return  _.sortBy(collection, functionType);
    };


}


let _My = new MyUtility;
let va = {'aa':15,'ba':16,'ca':1,'da':16,};
let value = {'a':1,'b':1,'c':1,'d':1,};
let value2 = _My.cloneDeep(value)
value2.a = 5

console.log(_My.assign(va,value2))
//_My.assign(value,value2)
console.log(value)
console.log(value2)