
const {Employee} = require('./Employee.js')
const {Programmer} = require('./Programmer.js')
const {OfficeCleaner} = require('./OfficeCleaner.js')
const fs = require('fs');

class CEO extends Employee {
    constructor(firstname, lastname, salary,id,dressCode) {
        super(firstname, lastname, salary);
        this.id = id
        this.dressCode = dressCode;
        this.employeesRaw =null
        this.employee= null
        let self = this;
       
    }
    getSalary() { // simulate public method
        return super.getSalary() * 2;
    };
    work(employee) { // simulate public method
        this._fire(employee);
        this._hire(employee);
        this._seminar();
        this._golf();
    }

    increaseSalary(employee, newSalary) {
        employee.setSalary(newSalary)

        if (employee.setSalary(newSalary) == false) {
            console.log(`${employee.firstname}'s salary is less than before!! `)
        } else {
            console.log(`${employee.firstname}'s salary has been set to ${newSalary}`)
        }
    }
    _fire(employee) {
        this.dressCode = 'tshirt';
        console.log(`${employee.firstname} has been fired! Dress with :${this.dressCode}`)
    }
    _hire(employee) {

        console.log(`${employee.firstname} has been hired back! Dress with :${this.dressCode} `)
    }
    _seminar() {
        this.dressCode = 'suit'
        console.log(`He is going to seminar Dress with :${this.dressCode}`)
    }
    _golf() { // simulate private method
        this.dressCode = 'golf_dress';
        console.log("He goes to golf club to find a new connection." + " Dress with :" + this.dressCode);

    };
 //-----------------------------------Read---------------------------------//
    myReadFile(filename) {
        return new Promise((resolve, reject) => {
            fs.readFile(filename, 'utf8', (error, data) => {
                if (error) reject(error);
                else {
                    resolve(data);
                }
            });
        });
    };
    
    async readData(filename,callback) {
        try {
            let dataRead = await this.myReadFile(filename);
            this.employeesRaw = JSON.parse(dataRead)
            // callback(null,this.employeesRaw)
            this.createEmployee(this.employeesRaw)
            callback(null,this.employee)

        } catch (error) {
            console.error(error)
        }
    }

//-----------------------------------------------------------------------//

createEmployee(data){
        this.employee = []
        for (let item = 0; item < data.length; item++) {
    
            if (data[item].role === 'CEO') {
                this.employee[item] = new CEO(
                    data[item].firstname, 
                    data[item].lastname, 
                    data[item].salary, 
                    data[item].id, 
                    data[item].dressCode)
            } else if (data[item].role === 'Programmer') {
                this.employee[item] = new Programmer(
                    data[item].firstname, 
                    data[item].lastname, 
                    data[item].salary, 
                    data[item].id, 
                    data[item].type)
            } else if (data[item].role == 'OfficeCleaner') {
                this.employee[item] = new OfficeCleaner(
                    data[item].firstname, 
                    data[item].lastname, 
                    data[item].salary, 
                    data[item].id, 
                    data[item].dressCode)
            }  
            
        }
        return this.employee
}








talk(message){
    console.log(message);
}

reportRobot(self,robotMessage)
{
    self.talk(robotMessage);
} 




}

exports.CEO = CEO