const {Employee} = require('./Employee.js');
const fs = require('fs');

class CEO extends Employee {
    constructor(firstname, lastname, salary) {
        super(firstname, lastname, salary);
        this.dressCode = 'suit';
        this.employeesRaw =[]
       
    }

    getSalary() { // simulate public method
        return super.getSalary() * 2;
    };
    work(employee) { // simulate public method
        this._fire(employee);
        this._hire(employee);
        this._seminar();
        this._golf();
    }
    increaseSalary(employee, newSalary) {
        employee.setSalary(newSalary)

        if (employee.setSalary(newSalary) == false) {
            console.log(`${employee.firstname}'s salary is less than before!! `)
        } else {
            console.log(`${employee.firstname}'s salary has been set to ${newSalary}`)
        }
    }
    _fire(employee) {
        this.dressCode = 'tshirt';
        console.log(`${employee.firstname} has been fired! Dress with :${this.dressCode}`)
    }
    _hire(employee) {

        console.log(`${employee.firstname} has been hired back! Dress with :${this.dressCode} `)
    }
    _seminar() {
        this.dressCode = 'suit'
        console.log(`He is going to seminar Dress with :${this.dressCode}`)
    }
    _golf() { // simulate private method
        this.dressCode = 'golf_dress';
        console.log("He goes to golf club to find a new connection." + " Dress with :" + this.dressCode);

    };
//------------------------------ReadFile------------------------------------------------//
    myReadFile(filename) {
        return new Promise((resolve, reject) => {
            fs.readFile(filename, 'utf8', (error, data) => {
                if (error) reject(error);
                else {
                    resolve(data);
                }
            });
        });
    };
    
    async readData(filename,callback) {
        try {
            let dataRead = await this.myReadFile(filename);
            //console.log(dataRead)
            this.employeesRaw = JSON.parse(dataRead)
            callback(null,this.employeesRaw)
            //console.log(dataRead)
           // return dataRead
        } catch (error) {
            console.error(error)
        }
    }
//----------------------------------------------------------------------------------------------//
}

exports.CEO = CEO