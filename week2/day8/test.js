
const fs = require('fs')

function myReadFile(filename) {
    return new Promise((resolve, reject) => {
        fs.readFile(filename, 'utf8', (error, data) => {
            if (error) reject(error);
            else {
                resolve(data);
                
            }
        });
    });
};

async function read(filename){
    try { 
        let dataRead = await myReadFile(filename);
        //console.log(dataRead)
        dataRead = JSON.parse(dataRead)
        return dataRead
    }catch (error){
        console.error(error)
    }
}

read('homework1.json')

