class MobilePhome{
    PhoneCall(){ } 
    SMS(){ } 
    InternetSurfing(){ } 
}
class Samsung extends MobilePhome{
    UseGearVR(){ }
    TransformToPC(){ }
    GooglePlay(){ }
}

let samsungGalaxyS8 = new Samsung()

class SamSungGalaxyNote8 extends Samsung {
    UsePen(){ }
}

class iPhone extends MobilePhome{
    AppStore(){   }
}

class iPhone8 extends iPhone{
    FaceID(){ }
}
class iPhoneX extends iPhone{
    TouchID(){    }
}



// SamSungGalaxy Note 8 -> UseGearVR(), TransformToPC(), UsePen(), GooglePlay() 
// iPhoneX-> FaceTime(), AppStore() 
// iPhone8 -> TouchID(), AppStore() 
// SamsungGalaxy S8 -> UseGearVR(), TransformToPC(), GooglePlay()