import $ from 'jquery'
import { fetchApi } from './middlewares'
import bookPage from './views/bookPage.html'
import bookItems from './views/bookItem.html'
import { templateBinder } from './lib/template'

$('#app').html(bookPage)
let api = new fetchApi()

api.get('http://localhost:3000/books')
.then(res => {
  res.json().then(result => {
    let template = templateBinder.bind(bookItems, result)
    $('#bookList').append(template)
  })
})

