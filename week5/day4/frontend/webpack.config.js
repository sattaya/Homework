var path = require('path')
module.exports = {
  devServer:{
    port:3000
  },
 entry: path.resolve('src/lib/index.js'),
 output: {
   filename: 'bundle.js',
   path: path.resolve('dist'),
   publicPath: 'dist'
 },
 module: {
   rules: [
     {
       test: /\.js$/,
       use: ['babel-loader'],
     },
     {
      test: /\.css$/,
      use: ['style-loader', 'css-loader'],
     },     
   ],
 }
}

