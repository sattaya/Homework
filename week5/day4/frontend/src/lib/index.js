

window.editButton = editButton
window.removeButton = removeButton
window.onload = function () {
   //SaveAdd
   let saveadd = document.getElementById('saveadd')
   saveadd.addEventListener('click', () => {
       getInfo()
    })
// let saveadd = document.getElementById('saveadd')
//     saveadd.addEventListener('click', () => {
//         getInfo()
//      })
}

function editButton(id) {
console.log(id)
}
function removeButton(id){
   // let productBox = getElementById(id)
   // document.removeChild(id)

    var r = confirm('Are you sure to delete item '+id+' y/n ')
    if(r == true){
        let elem = document.getElementById(id);
        let parent = document.getElementById("productRow");
        parent.removeChild(elem);
    }
}

function getInfo() {
    let bookID = document.getElementById('bookID')
    let tiTle = document.getElementById('tiTle')
    let price = document.getElementById('price')
    let promotionDate = document.getElementById('promotionDate')
    let image = document.getElementById('image')

    console.log(`
     bookID = ${bookID.value}
     tiTle = ${tiTle.value}
     price = ${price.value}
     promotionDate = ${promotionDate.value}
     image = ${image.value}
     `)

    let productRow = document.getElementById('productRow')
    productRow.innerHTML += `<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3  box" align="center" id="${bookID.value}">
    <div class="box1">
        <h3>${tiTle.value}</h3>
        <img src="${image.value}" >
        <div class="detail">Price: ${price.value}$</div>
            <div class="detailButton">
                <button class="btn btn-primary" type="button"  data-toggle="tooltip" data-placement="bottom" title="Add to Cart">
                    <span>
                        <i class="fas fa-shopping-cart"></i>
                    </span>
                </button>
                    <button class="btn btn-primary" type="button" title="Edit Product"  data-toggle="modal" data-target="#myModal" onclick="editButton('${bookID.value}')">
                            <span>
                                <i class="glyphicon glyphicon-cog"></i>
                            </span>
                    </button>
                    <button class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="bottom" title="Remove Product" onclick="removeButton('${bookID.value}')">
                            <span>
                                <i class="glyphicon glyphicon-remove"></i>
                            </span>
                    </button>
            </div>
    </div>
</div>
`


//   //Clear value in modal
//   document.getElementById("bookID").value = "";
//   document.getElementById("tiTle").value = "";
//   document.getElementById("price").value = "";
//   document.getElementById("promotionDate").value = "";
//   document.getElementById("image").value = "";

}

