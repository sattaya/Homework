/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "dist";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


function getInfo() {
    var bookID = document.getElementById('bookID');
    var tiTle = document.getElementById('tiTle');
    var price = document.getElementById('price');
    var promotionDate = document.getElementById('promotionDate');
    var image = document.getElementById('image');

    console.log('\n     bookID = ' + bookID.value + '\n     tiTle = ' + tiTle.value + '\n     price = ' + price.value + '\n     promotionDate = ' + promotionDate.value + '\n     image = ' + image.value + '\n     ');
}
//     let productRow = document.getElementById('productRow')
//     productRow.innerHTML += `<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3  box" align="center" id="productBox">
//     <div class="box1">
//         <h3>${tiTle.value}</h3>
//         <img src="../img/a.jpg" >
//         <div class="detail">Price: ${price.value}$</div>
//             <div class="detailButton">
//                 <button class="btn btn-primary" type="button"  data-toggle="tooltip" data-placement="bottom" title="Add to Cart">
//                     <span>
//                         <i class="fas fa-shopping-cart"></i>
//                     </span>
//                 </button>
//                     <button class="btn btn-primary" type="button" title="Edit Product"  data-toggle="modal" data-target="#myModal" onclick="editButton('${bookID.value}')">
//                             <span>
//                                 <i class="glyphicon glyphicon-cog"></i>
//                             </span>
//                     </button>
//                 <button class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="bottom" title="Remove Product">
//                         <span>
//                             <i class="glyphicon glyphicon-remove"></i>
//                         </span>
//                 </button>
//             </div>
//     </div>
// </div>
// `
// }

// function editButton(id) {
//     console.log(id);
// }

// window.editButton = editButton
// window.onload = function () {
//SaveAdd
var saveadd = document.getElementById('saveadd');
saveadd.addEventListener('click', function () {
    getInfo();
});

// }

/***/ })
/******/ ]);