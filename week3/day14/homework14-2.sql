-- categoryBook (id,name);
-- book (ISBN,name,stock,price);
-- publisher (id,name,address,phoneNumber,email);
-- author (id,name,address,email);
-- employees (id,name,address,phoneNumber,email,position,salary);
-- ordersale (id,date_sale,amount,price );
-- customer (id,name,address,phoneNumber,email);

-- -----------------------------------------------------
-- Table categoryBook 
-- -----------------------------------------------------
CREATE TABLE  categoryBook (
  `id` INT NOT NULL,
  `name` VARCHAR(255) NULL,
  PRIMARY KEY (`id`));


-- -----------------------------------------------------
-- Table book`
-- -----------------------------------------------------
CREATE TABLE book (
  `ISBN` INT NOT NULL,
  `name` VARCHAR(255) NULL,
  `stock` INT NULL,
  `price` DOUBLE NULL,
  PRIMARY KEY (`ISBN`),
  UNIQUE INDEX `ISBN_UNIQUE` (`ISBN` ASC));


-- -----------------------------------------------------
-- Table publisher`
-- -----------------------------------------------------
CREATE TABLE publisher (
  `id` INT NOT NULL,
  `name` VARCHAR(255) NULL,
  `address` TEXT NULL,
  `phoneNumber` VARCHAR(10) NULL,
  `email` VARCHAR(255) NULL,
  PRIMARY KEY (`id`));


-- -----------------------------------------------------
-- Table author`
-- -----------------------------------------------------
CREATE TABLE author (
  `id` INT NOT NULL,
  `name` VARCHAR(255) NULL,
  `address` TEXT NULL,
  `email` VARCHAR(255) NULL,
  PRIMARY KEY (`id`));


-- -----------------------------------------------------
-- Table employees`
-- -----------------------------------------------------
CREATE TABLE employees (
  `id` INT NOT NULL,
  `name` VARCHAR(255) NULL,
  `address` TEXT NULL,
  `phoneNumber` VARCHAR(10) NULL,
  `email` VARCHAR(255) NULL,
  `position` VARCHAR(255) NULL,
  `salary` DOUBLE NULL,
  PRIMARY KEY (`id`));


-- -----------------------------------------------------
-- Table ordersale`
-- -----------------------------------------------------
CREATE TABLE ordersale (
  `id` INT NOT NULL,
  `date_sale` TIMESTAMP NULL,
  `amount` INT NULL,
  `price` DOUBLE NULL,
  PRIMARY KEY (`id`));


-- -----------------------------------------------------
-- Table `customer`
-- -----------------------------------------------------
CREATE TABLE customer (
  `id` INT NOT NULL,
  `name` VARCHAR(255) NULL,
  `address` VARCHAR(255) NULL,
  `phoneNumber` VARCHAR(45) NULL,
  `email` VARCHAR(45) NULL,
  PRIMARY KEY (`id`));
