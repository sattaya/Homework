
--- จากการบ้านที่ 1 ข้อ 2 นักเรียนแต่ละคน ซื้อคอร์สไปคนละเท่าไร จำนวนกี่คอร์ส (ให้ใช้แค่ statement เดียวเท่านั้น)

select students.name AS StudentsName, count(enrolls.student_id) As course,
    sum(courses.price) AS Total from courses
    left join enrolls on enrolls.course_id = courses.id
    left join students on students.id = enrolls.student_id
    where enrolls.student_id is not null
    group by enrolls.student_id
    order by courses.id;
+-------------------+--------+-------+
| StudentsName      | course | Total |
+-------------------+--------+-------+
| Wulf Berg         |      1 |    90 |
| Barry Vaudrin     |      1 |    90 |
| Vaifra Melchiorri |      1 |    90 |
| Richard Cook      |      1 |    90 |
| Larry Berg        |      2 |   180 |
| Ken williams      |      1 |    90 |
| John Dougill      |      1 |    90 |
| Ken Cook          |      2 |   180 |
| Larry Cook        |      1 |    90 |
| Donald Greene     |      2 |   180 |
+-------------------+--------+-------+
10 rows in set (0.00 sec)

--- นักเรียนแต่ละคน ซื้อคอร์สไหน ราคาแพงสุด
select students.id,students.name,courses.name,max(courses.price) as maxPrice from enrolls
inner join students on students.id = enrolls.student_id
inner join courses on courses.id = enrolls.course_id
WHERE courses.price = (
    select max(c.price) as maxPrice 
    from enrolls e
    inner join students s on s.id = e.student_id
    inner join courses c on c.id = e.course_id 
    WHERE s.id = students.id)
    group by students.name
    order by students.id
+----+-------------------+------------------------+----------+
| id | name              | name                   | maxPrice |
+----+-------------------+------------------------+----------+
|  1 | Donald Greene     | Screenwriting          |       90 |
|  2 | Richard Cook      | Conservation           |       90 |
|  3 | John Dougill      | Singing                |       90 |
|  4 | Barry Vaudrin     | Acting                 |       90 |
|  5 | Vaifra Melchiorri | Chess                  |       90 |
|  6 | Ken williams      | Tennis                 |       90 |
|  7 | Larry Cook        | Dramatic Writing       |       90 |
|  8 | Wulf Berg         | Cooking                |       90 |
|  9 | Larry Berg        | Writing for Television |      200 |
| 10 | Ken Cook          | Film Scoring           |      100 |
+----+-------------------+------------------------+----------+
10 rows in set (0.00 sec)
