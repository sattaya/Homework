--- จากคอร์สที่มีคนเรียนทั้งหมด ได้เงินเท่าไร

MariaDB [couse]> select sum(courses.price) as TotalRevenue
    from courses
    left join enrolls on enrolls.course_id = courses.id
    where enrolls.student_id is not null
    order by courses.id;
+--------------+
| TotalRevenue |
+--------------+
|         1170 |
+--------------+
1 row in set (0.00 sec)


--- นักเรียนแต่ละคน ซื้อคอร์สไปคนละเท่าไร
MariaDB [couse]> select students.name AS StudentsName, sum(courses.price) AS Total from courses
    left join enrolls on enrolls.course_id = courses.id
    left join students on students.id = enrolls.student_id
    where enrolls.student_id is not null
    group by enrolls.student_id
    order by courses.id;
+-------------------+-------+
| StudentsName      | Total |
+-------------------+-------+
| Wulf Berg         |    90 |
| Barry Vaudrin     |    90 |
| Vaifra Melchiorri |    90 |
| Richard Cook      |    90 |
| Ken williams      |    90 |
| Larry Berg        |   180 |
| John Dougill      |    90 |
| Ken Cook          |   180 |
| Larry Cook        |    90 |
| Donald Greene     |   180 |
+-------------------+-------+
10 rows in set (0.00 sec)