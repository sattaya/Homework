
MariaDB [couse]> select distinct courses.id as Courses_ID,
    ->  courses.name as Coureses_name,
    ->  courses.price as Price,
    ->  instructors.name as Teachere
    ->  from courses
    ->  left join enrolls on enrolls.course_id = courses.id
    ->  left join instructors on instructors.id = courses.teach_by
    ->  where enrolls.student_id is not null
    ->  order by courses.id;
+------------+------------------------+-------+--------------------+
| Courses_ID | Coureses_name          | Price | Teachere           |
+------------+------------------------+-------+--------------------+
|          1 | Cooking                |    90 | Wolfgang Puck      |
|          2 | Acting                 |    90 | Samuel L. Jackson  |
|          3 | Chess                  |    90 | Garry Kasparov     |
|          5 | Conservation           |    90 | Dr. Jane Goodall   |
|          6 | Tennis                 |    90 | Serena Williams    |
|         11 | Singing                |    90 | Christina Aguilera |
|         15 | Film Scoring           |    90 | Hans Zimmer        |
|         17 | Writing for Television |    90 | Shonda Rhimes      |
|         19 | Dramatic Writing       |    90 | David Mamet        |
|         20 | Screenwriting          |    90 | Aaron Sorkin       |
|         24 | Photography            |    90 | Annie Leibovitz    |
+------------+------------------------+-------+--------------------+
11 rows in set (0.00 sec)

MariaDB [couse]> select distinct courses.id as Courses_ID,
    ->  courses.name as Coureses_name,
    ->  courses.price as Price,
    ->  instructors.name as Teachere
    ->  from courses
    ->  left join enrolls on enrolls.course_id = courses.id
    ->  left join instructors on instructors.id = courses.teach_by
    ->  where enrolls.student_id is null
    ->  order by courses.id;
+------------+-------------------------------------+-------+-----------------------+
| Courses_ID | Coureses_name                       | Price | Teachere              |
+------------+-------------------------------------+-------+-----------------------+
|          4 | Writing                             |    90 | Judy Blume            |
|          7 | The Art of Performance              |    90 | Usher                 |
|          8 | Writing #2                          |    90 | James Patterson       |
|          9 | Building a Fashion Brand            |    90 | Diane Von Furstenberg |
|         10 | Design and Architecture             |    90 | Frank Gehry           |
|         12 | Jazz                                |    90 | Herbie Hancock        |
|         13 | Country Music                       |    90 | Reba Mcentire         |
|         14 | Fashion Design                      |    90 | Marc Jacobs           |
|         16 | Comedy                              |    90 | Steve Martin          |
|         18 | Filmmaking                          |    90 | Werner Herzog         |
|         21 | Electronic Music Production         |    90 | Deadmau5              |
|         22 | Cooking #2                          |    90 | Gordon Ramsay         |
|         23 | Shooting, Ball Handler, and Scoring |    90 | Stephen Curry         |
|         25 | Database System Concept             |    30 | NULL                  |
|         26 | JavaScript for Beginner             |    20 | NULL                  |
|         27 | OWASP Top 10                        |    75 | NULL                  |
+------------+-------------------------------------+-------+-----------------------+
16 rows in set (0.00 sec)



MariaDB [couse]> select distinct courses.id as Courses_ID,
    ->  courses.name as Coureses_name,
    ->  courses.price as Price,
    ->  instructors.name as Teachere
    ->  from courses
    ->  left join enrolls on enrolls.course_id = courses.id
    ->  left join instructors on instructors.id = courses.teach_by
    ->  where enrolls.student_id is null && courses.teach_by is not null
    ->  order by courses.id;
+------------+-------------------------------------+-------+-----------------------+
| Courses_ID | Coureses_name                       | Price | Teachere              |
+------------+-------------------------------------+-------+-----------------------+
|          4 | Writing                             |    90 | Judy Blume            |
|          7 | The Art of Performance              |    90 | Usher                 |
|          8 | Writing #2                          |    90 | James Patterson       |
|          9 | Building a Fashion Brand            |    90 | Diane Von Furstenberg |
|         10 | Design and Architecture             |    90 | Frank Gehry           |
|         12 | Jazz                                |    90 | Herbie Hancock        |
|         13 | Country Music                       |    90 | Reba Mcentire         |
|         14 | Fashion Design                      |    90 | Marc Jacobs           |
|         16 | Comedy                              |    90 | Steve Martin          |
|         18 | Filmmaking                          |    90 | Werner Herzog         |
|         21 | Electronic Music Production         |    90 | Deadmau5              |
|         22 | Cooking #2                          |    90 | Gordon Ramsay         |
|         23 | Shooting, Ball Handler, and Scoring |    90 | Stephen Curry         |
+------------+-------------------------------------+-------+-----------------------+
13 rows in set (0.00 sec)

