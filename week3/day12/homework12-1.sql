
MariaDB [couse]> select courses.id, courses.name as course_name, instructors.name as instructor_name
    -> from courses
    -> right join instructors  on instructors.id = courses.teach_by
    -> where courses.id is null
    -> order by courses.id;
+------+-------------+------------------+
| id   | course_name | instructor_name  |
+------+-------------+------------------+
| NULL | NULL        | Martin Scorsese  |
| NULL | NULL        | Ron Howard       |
| NULL | NULL        | Alice Waters     |
| NULL | NULL        | Armin Van Buuren |
| NULL | NULL        | Bob Woofward     |
| NULL | NULL        | Thomas Keller    |
| NULL | NULL        | Helen Mirren     |
+------+-------------+------------------+
7 rows in set (0.00 sec)

MariaDB [couse]> select courses.id, courses.name as course_name, instructors.name as instructor_name
    -> from courses
    -> left join instructors  on instructors.id = courses.teach_by
    -> where instructors.id is null
    -> order by courses.id;
+----+-------------------------+-----------------+
| id | course_name             | instructor_name |
+----+-------------------------+-----------------+
| 25 | Database System Concept | NULL            |
| 26 | JavaScript for Beginner | NULL            |
| 27 | OWASP Top 10            | NULL            |
+----+-------------------------+-----------------+
3 rows in set (0.00 sec)

