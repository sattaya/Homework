MariaDB [couse]> insert into students(name) values
    -> ('Donald Greene'),
    -> ('Richard Cook'),
    -> ('John Dougill'),
    -> ('Barry Vaudrin'),
    -> ('Vaifra Melchiorri'),
    -> ('Ken williams'),
    -> ('Larry Cook'),
    -> ('Wulf Berg'),
    -> ('Larry Berg'),
    -> ('Ken Cook');
Query OK, 10 rows affected (0.13 sec)
Records: 10  Duplicates: 0  Warnings: 0

MariaDB [couse]> select * from students;
+----+-------------------+---------------------+
| id | name              | created_at          |
+----+-------------------+---------------------+
|  1 | Donald Greene     | 2018-01-09 14:41:25 |
|  2 | Richard Cook      | 2018-01-09 14:41:25 |
|  3 | John Dougill      | 2018-01-09 14:41:25 |
|  4 | Barry Vaudrin     | 2018-01-09 14:41:25 |
|  5 | Vaifra Melchiorri | 2018-01-09 14:41:25 |
|  6 | Ken williams      | 2018-01-09 14:41:25 |
|  7 | Larry Cook        | 2018-01-09 14:41:25 |
|  8 | Wulf Berg         | 2018-01-09 14:41:25 |
|  9 | Larry Berg        | 2018-01-09 14:41:25 |
| 10 | Ken Cook          | 2018-01-09 14:41:25 |
+----+-------------------+---------------------+
10 rows in set (0.00 sec)



MariaDB [couse]> insert into enrolls(student_id,course_id) values
    -> (1,24), (1,20), (2,5), (3,11), (8,1),
    -> (10,15), (9,17),(5,3), (7,19), (6,6),
    -> (4,2), (10,24), (9,6);
Query OK, 11 rows affected (0.13 sec)
Records: 11  Duplicates: 0  Warnings: 0

MariaDB [couse]> select * from enrolls;
+------------+-----------+---------------------+
| student_id | course_id | enrolled_at         |
+------------+-----------+---------------------+
|          1 |        20 | 2018-01-09 15:28:03 |
|          1 |        24 | 2018-01-09 15:28:03 |
|          2 |         5 | 2018-01-09 15:28:03 |
|          3 |        11 | 2018-01-09 15:28:03 |
|          4 |         2 | 2018-01-09 15:28:03 |
|          5 |         3 | 2018-01-09 15:28:03 |
|          6 |         6 | 2018-01-09 15:28:03 |
|          7 |        19 | 2018-01-09 15:28:03 |
|          8 |         1 | 2018-01-09 15:28:03 |
|          9 |         6 | 2018-01-09 15:30:28 |
|          9 |        17 | 2018-01-09 15:28:03 |
|         10 |        15 | 2018-01-09 15:28:03 |
|         10 |        24 | 2018-01-09 15:30:28 |
+------------+-----------+---------------------+
13 rows in set (0.00 sec)


MariaDB [couse]> select distinct courses.id as Courses_ID,
    ->  courses.name as Coureses_name
    ->  from courses
    ->  left join enrolls on enrolls.course_id = courses.id
    ->  where enrolls.student_id is not null
    ->  order by courses.id;
+------------+------------------------+
| Courses_ID | Coureses_name          |
+------------+------------------------+
|          1 | Cooking                |
|          2 | Acting                 |
|          3 | Chess                  |
|          5 | Conservation           |
|          6 | Tennis                 |
|         11 | Singing                |
|         15 | Film Scoring           |
|         17 | Writing for Television |
|         19 | Dramatic Writing       |
|         20 | Screenwriting          |
|         24 | Photography            |
+------------+------------------------+
11 rows in set (0.00 sec)


MariaDB [couse]> select distinct courses.id as Courses_ID,
    -> courses.name as Coureses_name
    -> from courses
    -> left join enrolls on enrolls.course_id = courses.id
    -> where enrolls.student_id is  null
    -> order by courses.id;
+------------+-------------------------------------+
| Courses_ID | Coureses_name                       |
+------------+-------------------------------------+
|          4 | Writing                             |
|          7 | The Art of Performance              |
|          8 | Writing #2                          |
|          9 | Building a Fashion Brand            |
|         10 | Design and Architecture             |
|         12 | Jazz                                |
|         13 | Country Music                       |
|         14 | Fashion Design                      |
|         16 | Comedy                              |
|         18 | Filmmaking                          |
|         21 | Electronic Music Production         |
|         22 | Cooking #2                          |
|         23 | Shooting, Ball Handler, and Scoring |
|         25 | Database System Concept             |
|         26 | JavaScript for Beginner             |
|         27 | OWASP Top 10                        |
+------------+-------------------------------------+
16 rows in set (0.00 sec)