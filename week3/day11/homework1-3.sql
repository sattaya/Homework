------------ Homework #1 -------------

MariaDB > create databse bookStore ; 
MariaDB > show databases;  
+--------------------+
| Database           |
+--------------------+
| bookstore          |
| center_service     |
| codecamp           |
| dbstudent          |
| icoffee            |
| indy_coffee        |
| information_schema |
| mysql              |
| performance_schema |
| phpmyadmin         |
| test               |
| website            |
+--------------------+

MariaDB [bookstore]> create table employees(id int auto_increment,
    ->  fristname varchar(100),
    ->  lastname varchar(100),
    ->  age int,
    ->  created_at timestamp default now(),
    ->  primary key (id));

MariaDB [bookstore]> describe employees;
+------------+--------------+------+-----+-------------------+----------------+
| Field      | Type         | Null | Key | Default           | Extra          |
+------------+--------------+------+-----+-------------------+----------------+
| id         | int(11)      | NO   | PRI | NULL              | auto_increment |
| fristname  | varchar(100) | YES  |     | NULL              |                |
| lastname   | varchar(100) | YES  |     | NULL              |                |
| age        | int(11)      | YES  |     | NULL              |                |
| created_at | timestamp    | NO   |     | CURRENT_TIMESTAMP |                |
+------------+--------------+------+-----+-------------------+----------------+
5 rows in set (0.69 sec)


MariaDB [bookstore]> create table book(ISBN varchar(100),
    ->	bookname varchar(255),
    ->	price double,
    ->	created_at timestamp default now(),
    ->	primary key (ISBN),
    ->	unique(ISBN));

MariaDB [bookstore]> describe book;
+------------+--------------+------+-----+-------------------+-------+
| Field      | Type         | Null | Key | Default           | Extra |
+------------+--------------+------+-----+-------------------+-------+
| ISBN       | varchar(100) | NO   | PRI | NULL              |       |
| bookname   | varchar(255) | YES  |     | NULL              |       |
| price      | double       | YES  |     | NULL              |       |
| created_at | timestamp    | NO   |     | CURRENT_TIMESTAMP |       |
+------------+--------------+------+-----+-------------------+-------+
4 rows in set (0.06 sec)

MariaDB [bookstore]> create table orderBook(id int auto_increment,
    -> ISBN varchar(100),
    -> bookname varchar(255),
    -> employee int,
    -> price double,
    -> volume int,
    -> orderDate timestamp default now()
    -> primary key (id));

MariaDB [bookstore]> describe orderbook;
+-----------+--------------+------+-----+-------------------+----------------+
| Field     | Type         | Null | Key | Default           | Extra          |
+-----------+--------------+------+-----+-------------------+----------------+
| id        | int(11)      | NO   | PRI | NULL              | auto_increment |
| ISBN      | varchar(100) | YES  |     | NULL              |                |
| bookname  | varchar(255) | YES  |     | NULL              |                |
| employee  | int(11)      | YES  |     | NULL              |                |
| price     | double       | YES  |     | NULL              |                |
| volume    | int(11)      | YES  |     | NULL              |                |
| orderDate | timestamp    | NO   |     | CURRENT_TIMESTAMP |                |
+-----------+--------------+------+-----+-------------------+----------------+
7 rows in set (0.04 sec)




------------ Homework #2 -------------

MariaDB [bookstore]> insert into employees (fristname,lastname,age)
    -> values ('Donald','Greene',25),
    -> ('Richard','Cook',30),
    -> ('John','Dougill',27),
    -> ('Barry','Vaudrin',25),
    -> ('Anjuman','Singh',33),
    -> ('vaifra','melchiorri',22),
    -> ('Ken','Williams',23),
    -> ('Larry','Immer',23),
    -> ('Wulf','Berg',25),
    -> ('Vincenzo','Berghella',24);

MariaDB [bookstore]> select * from employees;
+----+-----------+------------+------+---------------------+
| id | fristname | lastname   | age  | created_at          |
+----+-----------+------------+------+---------------------+
|  1 | Donald    | Greene     |   25 | 2018-01-09 09:59:49 |
|  2 | Richard   | Cook       |   30 | 2018-01-09 09:59:49 |
|  3 | John      | Dougill    |   27 | 2018-01-09 09:59:49 |
|  4 | Barry     | Vaudrin    |   25 | 2018-01-09 09:59:49 |
|  5 | Anjuman   | Singh      |   33 | 2018-01-09 09:59:49 |
|  6 | vaifra    | melchiorri |   22 | 2018-01-09 09:59:49 |
|  7 | Ken       | Williams   |   23 | 2018-01-09 09:59:49 |
|  8 | Larry     | Immer      |   23 | 2018-01-09 09:59:49 |
|  9 | Wulf      | Berg       |   25 | 2018-01-09 09:59:49 |
| 10 | Vincenzo  | Berghella  |   24 | 2018-01-09 09:59:49 |
+----+-----------+------------+------+---------------------+
10 rows in set (0.00 sec)

MariaDB [bookstore]> delete from employees where id = 5;
MariaDB [bookstore]> select * from employees;
+----+-----------+------------+------+---------------------+
| id | fristname | lastname   | age  | created_at          |
+----+-----------+------------+------+---------------------+
|  1 | Donald    | Greene     |   25 | 2018-01-09 09:59:49 |
|  2 | Richard   | Cook       |   30 | 2018-01-09 09:59:49 |
|  3 | John      | Dougill    |   27 | 2018-01-09 09:59:49 |
|  4 | Barry     | Vaudrin    |   25 | 2018-01-09 09:59:49 |
|  6 | vaifra    | melchiorri |   22 | 2018-01-09 09:59:49 |
|  7 | Ken       | Williams   |   23 | 2018-01-09 09:59:49 |
|  8 | Larry     | Immer      |   23 | 2018-01-09 09:59:49 |
|  9 | Wulf      | Berg       |   25 | 2018-01-09 09:59:49 |
| 10 | Vincenzo  | Berghella  |   24 | 2018-01-09 09:59:49 |
+----+-----------+------------+------+---------------------+
9 rows in set (0.00 sec)



MariaDB [bookstore]> alter table employees
    -> add address text default null;

MariaDB [bookstore]> select * from employees;
+----+-----------+------------+------+---------------------+---------+
| id | fristname | lastname   | age  | created_at          | address |
+----+-----------+------------+------+---------------------+---------+
|  1 | Donald    | Greene     |   25 | 2018-01-09 09:59:49 | NULL    |
|  2 | Richard   | Cook       |   30 | 2018-01-09 09:59:49 | NULL    |
|  3 | John      | Dougill    |   27 | 2018-01-09 09:59:49 | NULL    |
|  4 | Barry     | Vaudrin    |   25 | 2018-01-09 09:59:49 | NULL    |
|  6 | vaifra    | melchiorri |   22 | 2018-01-09 09:59:49 | NULL    |
|  7 | Ken       | Williams   |   23 | 2018-01-09 09:59:49 | NULL    |
|  8 | Larry     | Immer      |   23 | 2018-01-09 09:59:49 | NULL    |
|  9 | Wulf      | Berg       |   25 | 2018-01-09 09:59:49 | NULL    |
| 10 | Vincenzo  | Berghella  |   24 | 2018-01-09 09:59:49 | NULL    |
+----+-----------+------------+------+---------------------+---------+
9 rows in set (0.00 sec)

MariaDB [bookstore]> select * from employees;
+----+-----------+------------+------+---------------------+---------+
| id | fristname | lastname   | age  | created_at          | address |
+----+-----------+------------+------+---------------------+---------+
|  1 | Donald    | Greene     |   25 | 2018-01-09 09:59:49 |  123    |
|  2 | Richard   | Cook       |   30 | 2018-01-09 09:59:49 | NULL    |
|  3 | John      | Dougill    |   27 | 2018-01-09 09:59:49 | NULL    |
|  4 | Barry     | Vaudrin    |   25 | 2018-01-09 09:59:49 | NULL    |
|  6 | vaifra    | melchiorri |   22 | 2018-01-09 09:59:49 | NULL    |
|  7 | Ken       | Williams   |   23 | 2018-01-09 09:59:49 | NULL    |
|  8 | Larry     | Immer      |   23 | 2018-01-09 09:59:49 | NULL    |
|  9 | Wulf      | Berg       |   25 | 2018-01-09 09:59:49 | NULL    |
| 10 | Vincenzo  | Berghella  |   24 | 2018-01-09 09:59:49 | NULL    |
+----+-----------+------------+------+---------------------+---------+
9 rows in set (0.00 sec)

MariaDB [bookstore]> select count(*) from employees;
+----------+
| count(*) |
+----------+
|        9 |
+----------+
1 row in set (0.01 sec)

MariaDB [bookstore]> select * from employees where age < 20;
Empty set (0.01 sec)

MariaDB [bookstore]> select * from employees where age < 25;
+----+-----------+------------+------+---------------------+---------+
| id | fristname | lastname   | age  | created_at          | address |
+----+-----------+------------+------+---------------------+---------+
|  6 | vaifra    | melchiorri |   22 | 2018-01-09 09:59:49 | NULL    |
|  7 | Ken       | Williams   |   23 | 2018-01-09 09:59:49 | NULL    |
|  8 | Larry     | Immer      |   23 | 2018-01-09 09:59:49 | NULL    |
| 10 | Vincenzo  | Berghella  |   24 | 2018-01-09 09:59:49 | NULL    |
+----+-----------+------------+------+---------------------+---------+
4 rows in set (0.00 sec)




------------ Homework #3 -------------

MariaDB [bookstore]> insert into book (ISBN,bookname,price) values
    -> ('A001','How Not to Save the World',199),
    -> ('A002','Tarzan on the Precipice',250),
    -> ('A003','Francis of the Filth',350),
    -> ('A004','Computer 1',299),
    -> ('A005','Computer 2',250),
    -> ('A006','Computer 3',359),
    -> ('A007','Computer 4',590),
    -> ('A008','Home Security Systems',890),
    -> ('A009','Home Security Systems 2',990),
    -> ('A010','Home Security Systems 3',790);
Query OK, 10 rows affected (0.18 sec)
Records: 10  Duplicates: 0  Warnings: 0

MariaDB [bookstore]> select * from book;
+------+---------------------------+-------+---------------------+
| ISBN | bookname                  | price | created_at          |
+------+---------------------------+-------+---------------------+
| A001 | How Not to Save the World |   199 | 2018-01-09 11:37:06 |
| A002 | Tarzan on the Precipice   |   250 | 2018-01-09 11:37:06 |
| A003 | Francis of the Filth      |   350 | 2018-01-09 11:37:06 |
| A004 | Computer 1                |   299 | 2018-01-09 11:37:06 |
| A005 | Computer 2                |   250 | 2018-01-09 11:37:06 |
| A006 | Computer 3                |   359 | 2018-01-09 11:37:06 |
| A007 | Computer 4                |   590 | 2018-01-09 11:37:06 |
| A008 | Home Security Systems     |   890 | 2018-01-09 11:37:06 |
| A009 | Home Security Systems 2   |   990 | 2018-01-09 11:37:06 |
| A010 | Home Security Systems 3   |   790 | 2018-01-09 11:37:06 |
+------+---------------------------+-------+---------------------+
10 rows in set (0.00 sec)


MariaDB [bookstore]> select * from book  where bookname like '%Security%';
+------+-------------------------+-------+---------------------+
| ISBN | bookname                | price | created_at          |
+------+-------------------------+-------+---------------------+
| A008 | Home Security Systems   |   890 | 2018-01-09 11:37:06 |
| A009 | Home Security Systems 2 |   990 | 2018-01-09 11:37:06 |
| A010 | Home Security Systems 3 |   790 | 2018-01-09 11:37:06 |
+------+-------------------------+-------+---------------------+
3 rows in set (0.00 sec)

MariaDB [bookstore]> select * from book  where bookname like '%a%' order by  ISBN limit 4 ;
+------+---------------------------+-------+---------------------+
| ISBN | bookname                  | price | created_at          |
+------+---------------------------+-------+---------------------+
| A001 | How Not to Save the World |   199 | 2018-01-09 11:37:06 |
| A002 | Tarzan on the Precipice   |   250 | 2018-01-09 11:37:06 |
| A003 | Francis of the Filth      |   350 | 2018-01-09 11:37:06 |
+------+---------------------------+-------+---------------------+
3 rows in set (0.00 sec)


MariaDB [bookstore]> insert into orderbook(ISBN,bookname,employee,price,volume) values
    -> ('A004','Computer 1',1,299,2),
    -> ('A005','Computer 2',1,250,1),
    -> ('A006','Computer 3',1,359,10),
    -> ('A007','Computer 4',1,590,5),
    -> ('A003','Francis of the Filth',7,350,1),
    -> ('A009','Home Security Systems 2',7,990,2),
    -> ('A004','Computer 1',2,299,1),
    -> ('A004','Computer 1',3,299,5),
    -> ('A008','Home Security Systems',3,890,10),
    -> ('A010','Home Security Systems 3',9,790,15)
    -> ('A010','Home Security Systems 3',10,790,1),
    -> ('A008','Home Security Systems',2,890,2),
    -> ('A009','Home Security Systems 2',8,990,10),
    -> ('A003','Francis of the Filth',6,350,7),
    -> ('A003','Francis of the Filth',1,350,5),
    -> ('A003','Francis of the Filth',3,350,2),
    -> ('A006','Computer 3',10,359,5),
    -> ('A006','Computer 3',4,359,2),
    -> ('A005','Computer 2',6,250,10);

    
MariaDB [bookstore]> select * from orderbook;
+----+------+-------------------------+----------+-------+--------+---------------------+
| id | ISBN | bookname                | employee | price | volume | orderDate           |
+----+------+-------------------------+----------+-------+--------+---------------------+
|  1 | A004 | Computer 1              |        1 |   299 |      2 | 2018-01-09 12:00:46 |
|  2 | A005 | Computer 2              |        1 |   250 |      1 | 2018-01-09 12:00:46 |
|  3 | A006 | Computer 3              |        1 |   359 |     10 | 2018-01-09 12:00:46 |
|  4 | A007 | Computer 4              |        1 |   590 |      5 | 2018-01-09 12:00:46 |
|  5 | A003 | Francis of the Filth    |        7 |   350 |      1 | 2018-01-09 12:00:46 |
|  6 | A009 | Home Security Systems 2 |        7 |   990 |      2 | 2018-01-09 12:00:46 |
|  7 | A004 | Computer 1              |        2 |   299 |      1 | 2018-01-09 12:00:46 |
|  8 | A004 | Computer 1              |        3 |   299 |      5 | 2018-01-09 12:00:46 |
|  9 | A008 | Home Security Systems   |        3 |   890 |     10 | 2018-01-09 12:00:46 |
| 10 | A010 | Home Security Systems 3 |        9 |   790 |     15 | 2018-01-09 12:00:46 |
| 11 | A004 | Computer 1              |       10 |   299 |      5 | 2018-01-09 12:04:38 |
| 12 | A010 | Home Security Systems 3 |       10 |   790 |      1 | 2018-01-09 12:04:38 |
| 13 | A008 | Home Security Systems   |        2 |   890 |      2 | 2018-01-09 12:04:38 |
| 14 | A009 | Home Security Systems 2 |        8 |   990 |     10 | 2018-01-09 12:04:38 |
| 15 | A003 | Francis of the Filth    |        6 |   350 |      7 | 2018-01-09 12:04:38 |
| 16 | A003 | Francis of the Filth    |        1 |   350 |      5 | 2018-01-09 12:04:38 |
| 17 | A003 | Francis of the Filth    |        3 |   350 |      2 | 2018-01-09 12:04:38 |
| 18 | A006 | Computer 3              |       10 |   359 |      5 | 2018-01-09 12:04:38 |
| 19 | A006 | Computer 3              |        4 |   359 |      2 | 2018-01-09 12:04:38 |
| 20 | A005 | Computer 2              |        6 |   250 |     10 | 2018-01-09 12:04:38 |
+----+------+-------------------------+----------+-------+--------+---------------------+
20 rows in set (0.00 sec)

MariaDB [bookstore]> select sum(volume) from orderbook;
+-------------+
| sum(volume) |
+-------------+
|         101 |
+-------------+
1 row in set (0.00 sec)

MariaDB [bookstore]> select distinct ISBN from orderbook;
+------+
| ISBN |
+------+
| A004 |
| A005 |
| A006 |
| A007 |
| A003 |
| A009 |
| A008 |
| A010 |
+------+
8 rows in set (0.00 sec)


MariaDB [bookstore]> select sum(price * volume) from orderbook;
+---------------------+
| sum(price * volume) |
+---------------------+
|               56140 |
+---------------------+
1 row in set (0.00 sec)