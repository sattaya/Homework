function createEntity(row) {
    return {
        id: row.id,
        firstName: row.firstname,
        lastName: row.lastname
    }
}
async function find(db, id) {
    const [rows] = await db.execute(`select id,firstname, lastname from user where id = ?`, [id]) 
    return createEntity(rows[0])
}
async function findAll(db) {
    const [rows] = await db.execute(`select id,firstname, lastname from user `) 
    return rows.map(createEntity)
}
async function store(db, user) {
    if (!user.id) {
        const result = await db.execute(`insert into user (firstname, lastname) values ( ?, ? )`, 
        [user.firstName, user.lastName]) 
        user.id = result[0].insertId
        return
    } else {
        return db.execute(`update user set firstname = ?, lastname = ? where id = ?`, 
        [user.firstName, user.lastName, user.id])
    }
}
async function findByUsername(db,firstname) {
    const [rows] = await db.execute(`
      select id,firstname, lastname, salary, role from user where firstname = ?`, 
      [firstname])
    return createEntity(rows[0])
  }

function remove(db, id) {
    return db.execute(` delete from user where id = ? `, [id])
}

module.exports = {
    find,
    findAll,
    store,
    remove,
    findByUsername
}
