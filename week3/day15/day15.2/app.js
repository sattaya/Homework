const User = require('./repository/user')
const mysql2 = require('mysql2/promise')

const pool = mysql2.createPool({
    host: 'localhost',
    user: 'root',
    database: 'codecamp'
})

async function query() {
    const db = await pool.getConnection()
    await db.beginTransaction()

    const u = await User.find(db, 1002)
    //const u = {}
     u.firstName = 'Ton2Jon'
     u.lastName = 'test'
     await User.store(db, u)
    // const u = await User.findAll(db) 
     //const u = await User.remove(db,)
    // const u = await User.findByUsername(db,'Tony')

    console.log(u)
    await db.commit()
    await db.release()
}
query()