const render = require('koa-ejs')
const path = require('path')
const Koa = require('koa')
const app = new Koa()

const middlware = require('./controllers/routes.js')(app)

render(app,{
    root: path.join(__dirname,'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false,
    debug: true
})