class User {
  constructor(db, row) {
    this._db = db
    this.id = row.id
    this.firstname = row.firstname
    this.lastname = row.lastname
    this.salary = row.salary
    this.role = row.role
  }
  async save() {
    if (!this.id) {
      const result = await this._db.execute(`
        insert into user (
           firstname, lastname, salary, role
        ) values (
          ?, ?, ?, ?
        )
      `, [this.firstname, this.lastname, this.salary, this.role])
      this.id = result.insertId
      return
    }
    return this._db.execute(`
      update user set
        firstname = ?,
        lastname = ?,
        salary = ?,
        role = ?
      where id = ?
    `, [this.firstname, this.lastname, this.salary, this.role, this.id])
  }
  remove() {
    return this._db.execute(`
      delete from user where id = ?
    `, [this.id])
  }
}

module.exports = function (db) {
  return {
    async find(id) {
      try {
        const [rows] = await db.execute(`
          select
            id, firstname, lastname, salary, role
          from user
          where id = ?
        `, [id])
        return new User(db, rows[0])
      } catch(err) {
        return new User(db, {})
      }
    },
    async findAll() {
      const [rows] = await db.execute(`
        select
          id, firstname, lastname, salary, role
        from user
      `)
      return rows.map((row) => new User(db, row))
    },
    async findByUsername(username) {
      const [rows] = await db.execute(`
        select
          id,firstname, lastname, salary, role
        from user
        where firstname = ?
      `, [username])
      return new User(db, rows[0])
    }
  }
}