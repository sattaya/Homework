const config = require('../config/config.js')
const mysql2= require('mysql2/promise')

module.exports = function(){
    return mysql2.createPool(config)
}