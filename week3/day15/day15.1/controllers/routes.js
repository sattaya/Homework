const logger = require('koa-logger')
const db = require('../lib/db')()
const User = require('../model/user')(db)

module.exports = function (app) {
    app.use(logger())
    app.use(async(ctx,next) => {

        const user1 = await User.find(1001)
        
        user1.firstname = "Ton2"
        user1.lastname = "kla"
        // user1.salary = 50000
        // user1.role = "hacker"

        console.log(user1)

        await user1.remove()
        await user1.save()

        console.log(user1)

        // const user = await User.findAll()
        // let i = 0
        // let userArray = [];
        // user.reduce((prev, curr) => {
        //     userArray.push(curr.info)
        // },0)
        // await ctx.render('homework15', {
        //     "userRows" : userArray
        // })
        // await next();

        ctx.body = 'Hello'
        await next();

    })
    app.listen(3000)
}