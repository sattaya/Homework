class User {
    constructor(db, row) {
        this._db = db
        this.id = row.id
        this.firstName = row.first_name
        this.lastName = row.last_name
    }
    async save() {
        if (!this._id) {
            const result = await this._db.execute(`insert into users (first_name, last_name) values (?, ? )`, [this.firstName, this.lastName])
            this.id = result.insertId
            return
        }
        return this._db.execute(`update users set  first_name = ?,  last_name = ?  where id = ?`, [this.firstName, this.lastName, this.id])
    }
    async remove() {
        return this._db.execute(`delete from users where id = ?`, [this.id])
    }
    Active
}

module.exports = function (db) {
    return {
        async find(id) {
            const [rows] = await this.db.execute(`select first_name, last_name from users where id = ?`, [id]) 
            return new User(db, rows[0])
        },
        async findAll() {
            const [rows] = await this.db.execute(`select  first_name, last_name  from users`)
            return rows.map((row) => new User(db, row))
        }
    }
}