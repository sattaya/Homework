const {condb} = require('../config/conn.js')
class Database{
    constructor(host,user,database){
        this.host1 = host
        this.user1 = user
        this.database1 = database
        this.connectDatabase(host,user,database);

    }
    async connectDatabase(host1,user1,database1){
        const mysql = require('mysql2/promise'); 
        this.connection = await mysql.createConnection({
            host: this.host1,
            user: this.user1,
            database: this.database1
        });
    }
}

module.exports.db = new Database(condb.host,condb.user,condb.database);