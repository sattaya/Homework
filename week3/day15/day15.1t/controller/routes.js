const qury = require('../models/dbqury.js')
module.exports = function(app){
    app.use(async(ctx, next) => {
        try {
            const [rows,fields] = await qury.allUser()
            await ctx.render('homework15_1',{"rows":rows});
            
            await next();
        } catch (err) {
            ctx.status = 400
            ctx.body = `Uh-oh: ${err.message}`
        }
    });
}
