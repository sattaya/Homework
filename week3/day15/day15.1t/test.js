
function createEntity (row) {   
    return {id: row.id, firstName: row.first_name, lastName: row.last_name   } 
} 
async function find (db, id) {   
    const [rows] = await db.execute(`select firstname, lastname from user  where id = ?`, [id])   
    return createEntity(rows[0]) 
} 
async function findAll (db) {   
    const [rows] = await db.execute(`select firstname, lastname  from user`, [id])   
return rows.map(createEntity) }

async function store (db, user) {   
    if (!user.id) {     
        const result = await db.execute(`insert into user (firstname, lastname) values (?, ?)`, [user.firstName, user.lastName])     
        user.id = result.insertId     
        return   
    }   
    return db.execute(`update user  set  firstname = ?,lastname = ? where id = ?`, [user.firstName, user.lastName, user.id]) 
} 

function remove (db, id) {   
    return db.execute(`delete from user where id = ?   `, [id]) 
} 
module.exports = {find,findAll,store,remove}



const User = require('./repository/user') 
const user1 = await User.find(db, 1) 
user1.firstName = 'tester' 
await User.store(db, user1)

const mysql2 = require('mysql2/promise') 
const pool = mysql2.createPool({
    host: 'localhost',   
    user: 'root',   
    database: 'codecamp' }) 
    async function query () {   
        const db = await pool.getConnection()   
        await db.beginTransaction()   
        const u = await User.find(db, 1)   
        u.firstName = 'hacker'   
        await User.store(db, u)   
        await db.commit()   
        await db.release() 
    }