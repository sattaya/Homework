const Koa = require('koa')
const Router = require('koa-router')

const router = new Router()
// .get('/tweeter',(ctx) => { ctx.body = {status:"Wecome to tweeter!!",path : "get('/tweeter')"}})
const app = new Koa()
//Twitter Routes

//Auth
router.post('/auth/signup', (ctx, next) => { ctx.body = 'signup' })
router.post('/auth/signin', (ctx, next) => { ctx.body = 'signin' })
router.get('/auth/signout', (ctx, next) => { ctx.body = 'signout' })
router.post('/auth/verify', (ctx, next) => { ctx.body = 'verify' })

//Upload
router.post('/auth/upload', (ctx, next) => { ctx.body = 'upload' })

//User
router.patch('/user/:id', (ctx, next) => { ctx.body = 'Proflie' })
router.put('/user/:id/follow', (ctx, next) => { ctx.body = 'follow' })
router.delete('/user/:id/follow', (ctx, next) => { ctx.body = 'unfollow' })
router.get('/user/:id/follow', (ctx, next) => { ctx.body = 'follow' })
router.get('/user/:id/followed', (ctx, next) => { ctx.body = 'followed' })

//Tweet
router.get('/tweet', (ctx, next) => { ctx.body = 'show tweet' })
router.post('/tweet', (ctx, next) => { ctx.body = 'tweet' })
router.put('/tweet/:id/like', (ctx, next) => { ctx.body = 'like tweet' })
router.delete('/tweet/:id/like', (ctx, next) => { ctx.body = 'unlike tweet' })
router.post('/tweet/:id/retweet', (ctx, next) => { ctx.body = 'retweet' })
router.put('/tweet/:id/vote/:voteId', (ctx, next) => { ctx.body = 'vote' })
router.post('/tweet/:id/reply', (ctx, next) => { ctx.body = 'reply' })

//Notification
router.get('/notification', (ctx, next) => { ctx.body = 'notification' })

//Direct Message
router.get('/message', (ctx, next) => { ctx.body = 'message' })
router.get('/message/:userId', (ctx, next) => { ctx.body = 'show message' })
router.post('/message/:userId', (ctx, next) => { ctx.body = 'send message' })

app.use(requestLogger);
app.use(router.routes()).listen(3000);

async function requestLogger (ctx, next) {
    console.log(`${ctx.method} ${ctx.path}`)
    await next()
}
