module.exports = {
    create,
    list,
    find,
    changeContent,
    remove,
    markComplete,
    markIncomplete
  }
  
  async function create (db, todo) {
    let result = await db.execute('insert into todo (username,password) values (?,?)',[todo.username,todo.password])
  }
  
  async function list (db) {
   let [result] = await db.execute('select * from todo')
  return [result]
  }
  
  async function find (db, id) {
    let [result] = await db.execute('select * from todo where id = ?', [id])
    return result
  
  }
  
  async function changeContent (db, id, username) {
    let [result] = await db.execute('update todo set username = ? where id = ?',[username,id])
    return result
  }
  
  async function remove (db, id) {
    let [result] = await db.execute('delete from todo where id = ?', [id])
      return result
  }
  
  async function markComplete (db, id) {
    let [result] = await db.execute('update todo set status = 1 where id = ?', [id])
    return result
  }
  
  async function markIncomplete (db, id) {
   let [result] = await db.execute('update todo set status = 0 where id = ?', [id])
   return result
  }