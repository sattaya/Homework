module.exports = function (pool, repo) {
    return {
      async list (ctx) {
        ctx.body = await repo.list(pool)
       console.log(ctx.body)
      },
      async create (ctx) {
       // const todo = ctx.request.body
        // TODO: validate todo
        // const id = await repo.create(pool, todo)
        // ctx.body = { id }
        let todo = ctx.request.body;
        
            await repo.create(pool,todo);
            //ctx.redirect('/todo');
            ctx.body = `Create new User is complete`

      },
      async get (ctx) {
        const id = parseInt(ctx.params.id)
        // TODO: validate id
        // find todo from repo
        // send todo
        if (typeof id == 'number'){
          ctx.body =  await repo.find(pool,id)
        }else{
          ctx.body = `This is not ID. is type of ${typeof id}`
        }

      },
      async update (ctx) {
        const id = ctx.params.id
        const username = ctx.request.body.username
        await repo.changeContent(pool,id,username,)
        ctx.body = { status: "complete" }
      },
      async complete (ctx) {
        const id = ctx.params.id
        await repo.markComplete(pool,id)
       ctx.body = {status : "set complete"}
      },
      async incomplete (ctx) {
        const id = ctx.params.id
        await repo.markIncomplete(pool,id)
        ctx.body = {status : "set incomplete"}
      },
      async remove (ctx){
        const id = ctx.params.id
       await repo.remove(pool,id)
       ctx.body = {status : "remove complete"}
      },
    }
  }