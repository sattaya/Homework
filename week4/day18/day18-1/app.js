const Koa = require('koa') 
const multer = require('koa-multer') 
const jimp = require('jimp') 

const upload = multer({dest: 'upload/'}) 

const app = new Koa() 

app.use(async(ctx) => {
    await upload.single('file')(ctx) 
    const tempFile = ctx.req.file.path
    const outFile = tempFile + '.jpg'
    await jimp.read(tempFile).then( f => {
        f.resize(100, 100).write(outFile) 
        ctx.body = outFile
    })
    
}) 

app.listen(3000)