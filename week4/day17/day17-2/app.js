const Koa = require('koa')
const Router = require('koa-router')
const bodyParser = require('koa-bodyparser')
const mysql2 = require('mysql2/promise')
const bcrypt = require('bcryptjs');
///---------------------------------------///
const dbConfig = require('./config/db')
const maketwitCtrl = require('./ctrl/ctrl_tweiter.js')
const twitRepo = require('./repo/repo_tweiter')

const pool = mysql2.createPool(dbConfig)
const twitCtrl = maketwitCtrl(pool, bcrypt, twitRepo)

const router = new Router()
  .get('/twiter', twitCtrl.list)
  .post('/twiter/signup', twitCtrl.signup)

const app = new Koa()
app.use(bodyParser())
app.use(router.routes())
app.listen(3000)