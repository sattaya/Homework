module.exports = function (pool, bcrypt, repo) {
  return {
    async list(ctx) {
      ctx.body = await repo.list(pool)
      console.log(ctx.body)
    },
    async signup(ctx) {
      console.log(ctx.request.body);
      const data = ctx.request.body

      //console.log('Request Body:', ctx.request)
      if (!data.password) {
        ctx.throw(400);
      }
       data.password = await bcrypt.hash(data.password, 10);
      
      try {
        await  repo.create(pool, data);
        ctx.body = {}
      } catch (err) {
        ctx.body = {
          "error": "some error"
        }
      }
    }
  }
}