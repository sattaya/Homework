const Koa = require('koa');
const session = require('koa-session');
const sessionStore = {};
const {db} = require("./lib/db.js");
const mysess = require("./model/db.js")(db);

let check = 0;
const sessionConfig = {
    key: 'sess',
    maxAge: 3600 * 1000,
    httpOnly: true,
    store: mysess
};

const app = new Koa();
app.keys = ['supersecret'];
app.use(session(sessionConfig, app))
    .use(handler)
    .listen(3000);

function handler (ctx) {
    if(ctx.path != '/favicon.ico'){
        let n = ctx.session.views || 0
        ctx.session.views = ++n
        ctx.body = `${n} views`
    }
}


