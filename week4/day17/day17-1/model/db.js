module.exports = function (db) {
    return {
        async get(key, maxAge, {rolling}) {
            let result = await db.connection.execute("SELECT `sess_sess` FROM `sessionstore` WHERE `sess_key` = ?", [key]);
            if (Array.isArray(result)) {
                if (result.length > 0) {
                    result = result[0]
                    console.log(result[0].sess_sess);
                    let str = JSON.parse(result[0].sess_sess);

                    return str;
                }
            }
            return;
        },
        async set(key, sess, maxAge, {rolling}) {
            let str = JSON.stringify(sess);
            await db.connection.execute(`INSERT INTO sessionstore (sess_key, sess_sess) 
            VALUES (?,?) on duplicate key update sess_sess = ?`, 
            [key, str, str]);

        },
        async destroy(key) {
            await db.connection.execute("DELETE FROM `sessionstore` WHERE sess_key = ?", [key]);
            return;
        }
    }
}