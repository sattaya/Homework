let fs = require('fs')
function readhead() {
    return new Promise(function (resolve, reject) {
        fs.readFile('./robot/head.txt', 'utf8', function (err, head) {
            if (err) reject(err);
            else resolve(head);
        });
    });
}

function readbody() {
    return new Promise(function (resolve, reject) {
        fs.readFile('./robot/body.txt', 'utf8', function (err, body) {
            if (err) reject(err);
            else resolve(body);
        });
    });
}

function readleg() {
    return new Promise(function (resolve, reject) {
        fs.readFile('./robot/leg.txt', 'utf8', function (err, leg) {
            if (err) reject(err);
            else resolve(leg);
        });
    });
}

function readfeet() {
    return new Promise(function (resolve, reject) {
        fs.readFile('./robot/feet.txt', 'utf8', function (err, feet) {
            if (err) reject(err);
            else resolve(feet);
        });
    });s
}

function writerobot(head,body,leg,feet) {
    return new Promise(function (resolve, reject) {
        fs.writeFile('./robot/robotAwait.txt',head+"\n"+body+"\n"+leg+"\n"+feet, 'utf8', function (err) {
            if (err) reject(err);
            else resolve();
        });
    });
}

function readrobot() {
    return new Promise(function (resolve, reject) {
        fs.readFile('./robot/robotAwait.txt', 'utf8', function (err, robotAwait) {
            if (err) reject(err);
            else console.log(robotAwait);
        });
    });s
}




async function newrobot() {
    try {
        let datahead = await readhead();
        let databody = await readbody();
        let dataleg = await readleg();
        let datafeet = await readfeet();
        await writerobot(datahead,databody,dataleg,datafeet);
        readrobot()

    } catch (error) {
        console.error(error);
    }
}
newrobot();