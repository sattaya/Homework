let fs = require('fs')



let p1 = new Promise(function (resolve, reject) {
    fs.readFile('./robot/head.txt', 'utf8', function (err, head) {
        if (err) reject(err);
        else resolve(head);
    });
});

let p2 = new Promise(function (resolve, reject) {
    fs.readFile('./robot/body.txt', 'utf8', function (err, body) {
        if (err) reject(err);
        else resolve(body);
    });
});

let p3 = new Promise(function (resolve, reject) {
    fs.readFile('./robot/leg.txt', 'utf8', function (err, leg) {
        if (err) reject(err);
        else resolve(leg);
    });
});
let p4 = new Promise(function (resolve, reject) {
    fs.readFile('./robot/feet.txt', 'utf8', function (err, feet) {
        if (err) reject(err);
        else resolve(feet);
    });
});


Promise.all([p1, p2, p3, p4])
.then(function (result) {
    fs.writeFile('./robot/robot.txt',result[0]+"\n"+result[1]+"\n"+result[2]+"\n"+result[3], 'utf8', function (err) {
        fs.readFile('./robot/robot.txt', 'utf8', function (err, robot2) {
        console.log(robot2);
        });
    });


}).catch(function (error) {
    console.error("There's an error", error);
});